package com.codistan.java.bulent.lesson13;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class MapExamples {

	public static void main(String[] args) {
		
		HashMap<String, String> myHM = new HashMap<>();
		
		myHM.put("C1-2-23", "Harry Potter");
		myHM.put("F2-5-10", "Starwars");
		myHM.put("T5-1-21", "Java 8 Fundamentals");
		myHM.put("H9-2-67", "Elm Street");
		myHM.put("N-7-6-34", "NY Times");
		myHM.put("B6-4-45", "Bill Gates");
		
		System.out.println(myHM);
		System.out.println(myHM.get("T5-1-21"));
		
		System.out.println(myHM.values());
		System.out.println(myHM.containsValue("Starwars"));
		System.out.println(myHM.keySet());
		
		Set setView = myHM.entrySet();
		
		Iterator myIte = setView.iterator();
		
		while(myIte.hasNext()) {
			Map.Entry currentEntry = (Map.Entry) myIte.next();
			System.out.println("The key is " + currentEntry.getKey() + " and the element is " + currentEntry.getValue());
		}
		
				
	}

}
