package com.codistan.java.bulent.lesson13;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.TreeSet;

public class IteratorExamples {

	public static void main(String[] args) {
		
		ArrayList<Integer> myAL = new ArrayList<>();
		
		myAL.add(2);
		myAL.add(13);
		myAL.add(1);
		myAL.add(57);
		myAL.add(6);
		
		System.out.println(myAL);
		
		Iterator myIterator = myAL.iterator();
		
		while (myIterator.hasNext()) {
			System.out.println(myIterator.next());
		}
		
		ArrayList<String> list =new ArrayList<String>();
	    		
		String exStr = "Hello world, I would like to say hello everyone because the world needs greetings and one of the good greeting way is saying hello.";
		
		String s[]= exStr.split(" ");
		System.out.println(Arrays.toString(s));
		
		TreeSet<String> myTs = new TreeSet<>();
		
		for (int i = 0; i < s.length; i++) {
			
			myTs.add(s[i]);
			
		}
		
		System.out.println(myTs);
		
		Iterator mySt = myTs.iterator();
		
		while(mySt.hasNext()) {
			System.out.println(mySt.next());
		}  
		
		
}
	}
