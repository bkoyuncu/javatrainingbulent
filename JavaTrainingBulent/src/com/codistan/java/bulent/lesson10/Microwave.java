package com.codistan.java.bulent.lesson10;

public class Microwave {

	private final String color = "white"; // String color = new String("white");
	private final String brand = "Samsung";
	private final String model = "SLX100";
	private int powerConsumption = 100;
	static String purpose = "Cooking";
	public void setPowerConsumption(int powerLevel) {
		switch (powerLevel) {
		case 1:
			powerConsumption = 500;
			System.out.println("Cooking in low power.");
			break;
		case 2:
			powerConsumption = 1000; 
			System.out.println("Cooking in medium power.");
			break;
		case 3:
			powerConsumption = 1500; 
			System.out.println("Cooking in high power.");
			break;

		default:
			System.out.println("Wrong selection, please select 1 for low, 2 for medium, 3 for high.");
		}
		}

	public void setPowerConsumption() { //this is overloading
		powerConsumption = 1000;
	}

	public int getPowerConsumption(){
	return powerConsumption;
}


}