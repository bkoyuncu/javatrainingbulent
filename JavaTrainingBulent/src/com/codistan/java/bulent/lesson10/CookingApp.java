package com.codistan.java.bulent.lesson10;

public class CookingApp {

		public static void main(String[] args) {
			Microwave myFirstMW = new Microwave(); 
			System.out.println("Before changing the pupose" + myFirstMW.purpose);
			myFirstMW.purpose = "cleaning";
			System.out.println("After changing the purpose" + myFirstMW.purpose);
			
			myFirstMW.setPowerConsumption(1);
			myFirstMW.setPowerConsumption(2);
			myFirstMW.setPowerConsumption(3);
			myFirstMW.setPowerConsumption(4);
			
			myFirstMW.setPowerConsumption();
			System.out.println(myFirstMW.getPowerConsumption());
	}

}
