package com.codistan.java.bulent.lesson6;

public class casting {

	public static void main(String[] args) {
		// TODO Auto-generat
		
		int myInt = 5;
		double myDouble = myInt; //implicit casting
		int mySecondInt = (int) myDouble; //explicit casting
		
		System.out.println(myDouble);
		System.out.println(mySecondInt);
	}

}
