package com.codistan.java.bulent.lesson6;

public class JavaStrings {

	public static void main(String[] args) {

		String exampleString = "Hello World!";//literal String creation
				System.out.println(exampleString);
				
		String exaStringTwo = new String("Hello World!");
		exaStringTwo = new String ("Hello Class!");
		exaStringTwo = "Hello World!";
		
		exampleString = "Hello Class!";
		
		String exampleThree = "My name is ";
		exampleThree = exampleThree + "John";
		System.out.println(exampleThree);
		
		System.out.println(exampleThree.charAt(1));//it starts with 0
		System.out.println(exampleThree.concat("!!!"));//add !!!
		System.out.println(exampleThree);//String methods do not change string itself
		
		exampleThree = exampleThree.concat("!!!");
		System.out.println(exampleThree.contains("John"));//look fot the word in sentence
		
		
	}

}
