package com.codistan.java.bulent.lesson6;

public class JavaStringTwo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	
	String firstStr = "John";
	String secondStr = "John";
	String thirdStr = new String("John");
	String forthStr = new String("john");

   System.out.println(firstStr.equals(secondStr));//when you are comparing strings, always use equals().
   System.out.println(secondStr.equals(thirdStr));
   System.out.println(thirdStr.equals(forthStr));
   System.out.println(thirdStr.equalsIgnoreCase(forthStr));
   
   System.out.println(firstStr.indexOf("o"));
   System.out.println(firstStr.indexOf("hn"));
   System.out.println(firstStr.length());
   System.out.println(firstStr.replace("o", "oa"));
   
   String fifthStr = "Hello World";
   
   System.out.println(fifthStr.replaceAll("l", "t"));
   
   String [] strArray = fifthStr.split(" ");
   System.out.println(strArray[0]);
   System.out.println(strArray[1]);
   System.out.println(fifthStr.endsWith("o"));
   System.out.println(fifthStr.substring(4, 7));
   System.out.println(fifthStr.toUpperCase());
   
   String sixthStr = " John ";
   System.out.println(sixthStr);
   System.out.println(sixthStr.trim());
   
   String emptStr = new String(); //in memory
   String emptyStrTwo = ""; // in pool
   String anotherStr; //null
   
   
   String fullName = "John Adams";
   String[] strArray02 = fullName.split(" ");
   String fullNameTwo = String.join(" ", strArray02);
   System.out.println(fullNameTwo);
	}
	
}
