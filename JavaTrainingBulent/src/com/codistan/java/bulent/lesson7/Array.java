package com.codistan.java.bulent.lesson7;

public class Array {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int [] myIntegers = new int[4];
		
		myIntegers[0] = 10;
		myIntegers[1] = 20;
		myIntegers[2] = 70;
		myIntegers[3] = 40;

		//or
		
		int [] myIntegersTwo = {10, 20, 30, 40};
		
		
		System.out.println(myIntegers[0]);
		System.out.println(myIntegers[1]);
		System.out.println(myIntegersTwo[3]);
		
		System.out.println(myIntegersTwo.length);

		
		/*String[] shoppingList = {"Lemon", "Lettuce", "Watermelon", "Orange", "Spinach"};
		double itemPrice[] = {5.99, 2.99, 1.89, 8.00, 2.89};
		String str="The price of";
		
		System.out.println(str + " " + shoppingList[0] +  " is $" + itemPrice[0]);
		System.out.println(str + " " + shoppingList[1] +  " is $" + itemPrice[1]);
		System.out.println(str + " " + shoppingList[2] +  " is $" + itemPrice[2]);
		System.out.println(str + " " + shoppingList[3] +  " is $" + itemPrice[3]);
		System.out.println(str + " " + shoppingList[4] +  " is $" + itemPrice[4]);*/
		
	

		
		
		
		

	}

}
