package com.codistan.java.bulent.lesson7;

import java.util.Arrays;
//java.lang
public class ArraysExample {

	public static void main(String[] args) {

		int [] numbersOne = {76, 3, 65, 99, 87, 5};
		
		int [] numbersTwo = {76, 3, 65, 99, 87, 5};
		
		System.out.println(numbersOne[4]);
		
		System.out.println(numbersOne);//
		
		System.out.println(Arrays.toString(numbersOne));
		
		System.out.println(numbersOne == numbersTwo);//false because they are same
		Arrays.sort(numbersOne);
		System.out.println(Arrays.toString(numbersOne));
		
		System.out.println(Arrays.binarySearch(numbersOne, 5));//first, do sort and then find to index number
		
		int [] subArray = Arrays.copyOfRange(numbersOne, 0, 2);
		System.out.println(Arrays.toString(subArray));
		
		
	}

}
