package com.codistan.java.bulent.lesson7;

import java.util.ArrayList;


public class ArrayListExample {
	
	public static void main(String[] args) {
		
		ArrayList<String> myArrayList = new ArrayList<>();//how we create ArrayList
		
		myArrayList.add("Hello");
		myArrayList.add("World");
		myArrayList.add("!");
		System.out.println(myArrayList);
		
		myArrayList.remove("!");
		System.out.println(myArrayList);

		myArrayList.remove(0);
		System.out.println(myArrayList);

		System.out.println(myArrayList.size());
		
		System.out.println(myArrayList.get(0));
		
		System.out.println(myArrayList.contains("Hello"));
		System.out.println(myArrayList.contains("World"));
		System.out.println(myArrayList.indexOf("World"));
		
	//	myArrayList.clear();
		System.out.println(myArrayList);
		
		


	}

}
