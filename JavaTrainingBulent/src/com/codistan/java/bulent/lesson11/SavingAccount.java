package com.codistan.java.bulent.lesson11;

public class SavingAccount extends Account{
	
	//minimum amount on the account should be $10,000
	//if you withdraw more than $1000, you will be charged for $45. 
	
	public SavingAccount(String accountNo, String routingNo, String ownersFullName, double balance) {
		super(accountNo, routingNo, ownersFullName, balance);
		if(balance < 10000) {
			System.exit(0);
		}
	}

	protected void depositCash(double amount) {
//		balance = balance + amount; 
		super.setBalance(getBalance() + amount);
	}

	public void depositCheck(double amount) {
//		balance = balance + amount; 
		super.setBalance(getBalance() + amount);

	}

//	public void transfer() {
//		
//
//	}


	public void withdrawMoney(double amount) {
//		balance = balance - amount; 
		if (getBalance() - amount >= 10000) {
			super.setBalance(getBalance() - amount); //getBalance is inherited from the parent class.
			if(amount>1000) {
				super.setBalance(getBalance() - 45);
			}
		} else {
			System.out.println("Your balance can not be less than $10,000, please revise your amount.");
		}
	}

	public void setBalance(double balance) { //overriding the parent class method
		System.out.println("Setting Balance is not allowed.");
	}

}
