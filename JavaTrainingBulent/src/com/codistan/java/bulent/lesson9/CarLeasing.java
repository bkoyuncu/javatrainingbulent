package com.codistan.java.bulent.lesson9;

import java.util.Scanner;

public class CarLeasing {

	public static void main(String[] args) {
		//Create a method that will calculate the amount the user needs pay if s/he exceeds the allowed mileage which 36000. calculateExceesMileageCharge() will take odometer reading mileage as an input, will return the money amount that the user owes. Every exceed mileage will be calculated $0.15/mile.
	
		System.out.println(calculateExceesMileageCharge(40000));
		System.out.println(calculateExceesMileageCharge(10000));
		System.out.println(calculateExceesMileageCharge(36000));
//create a method that will calculate how much the user will pay in total at the end of the lease. totalPayment() which will take downpayment amount and also monthly payment and it will return the total amount that costumer will pay at the end of the lease.
		
		System.out.println(totalPayment(3000, 200));
}
	public static double calculateExceesMileageCharge(int mileage) {
		
		if(mileage > 36000) {
			return (mileage - 36000)*0.15;
		}else {
			return 0.0;
		}
		
	}
		
		public static double totalPayment(double downPayment, double monthlyPayment) {
			
			return (downPayment + (monthlyPayment*36));
		}
		
		
	}

