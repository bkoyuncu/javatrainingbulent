package com.codistan.java.bulent.lesson9;

import java.util.Scanner;

public class countDownApplication {

	public static void main(String[] args) throws InterruptedException{
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("How many seconds to count down from?");
		
		int seconds = myScanner.nextInt();
		
//		for (int i = seconds; i>=0; i--) {
//			System.out.println(i + " second left");
//			
//			Thread.sleep(2000);
//		}
		
		while(seconds > 0) {
			System.out.println(seconds + " seconds left");
			Thread.sleep(1000);
			seconds --;
		}
			System.out.println("Time is up!");
		}
	}


