package com.codistan.java.bulent.lesson9;

public class ForLoop {

	public static void main(String[] args) {

		/*first statement runs once before the loop
		second statement is the conditition that is checked in each loop
		third statement  runs at the end of each cycle
		*/
		for(int counter2 = 0; counter2 <=50; counter2+=2) {
		System.out.println("Current number with for-loop is " + counter2);
			}
			
		for(int i = 0; i <= 50; i+=5) {
			
			System.out.println("Current number with for-loop is " + i);
	}
		/*print numbers from 0 to 50, 5 by 5 with for loop,
		 * if the number can be divided by 30 then stop counting. Ex: 0, 5, 15, 20, ... 30
		 * skip number 10
		 */
		
		for (int i = 0; i < 50; i+=5) {
			if(i==10) {
				continue;
			}
			if (i==30) {
				break;
			}
			System.out.println(i);
			
		}
		
		String myStringArray[] = {"Hello", "World", "and", "Galaxy"};
		for(int i = 0; i < myStringArray.length; i++) {
			System.out.println(myStringArray[i].toUpperCase());
		}
		String myStringArray1[] = {"Hello", "World", "and", "Galaxy"}; //reverse 
		for(int i = myStringArray1.length -1; i >=0; i--) {
			System.out.println(myStringArray1[i]);

		}
		}
	
	//int x = 0;
	
	//for(initiation; condition; increment/decrement)
	  //  for(int i=0; i<2; i++) {
	    //	System.out.println("d");
	    
}


