package com.codistan.java.bulent.lesson9;

public class WhileExample2 {

	public static void main(String[] args) {

		int counter = 0;
		while (counter <= 100) {
			System.out.println("while" + counter);
			counter++;
		}

		counter = 0; // resetting counter
		do {
			System.out.println("do while" + counter);
			counter++;
		} while (counter <= 100);

		counter = 110;
		while (counter >= 100) {
			System.out.println(counter);
			counter--;
		}

		do {
			System.out.println(counter);
			counter--;
		} while (counter >= 100);

		counter = 100;
		while (counter <= 200) {
			System.out.println(counter);
			counter += 2;

		}
//Switch
		int dayOfTheWeek = 5;
		switch (dayOfTheWeek) {
		case 1:
			System.out.println("Monday");
			break; // get out of the block
			
		case 2:
			System.out.println("Tuesday");
			break;
			
		case 3:
			System.
			out.println("Wednesday");
			break;
			
		case 4:
			System.out.println("Thursday");
			break;
			
		case 5:
			System.out.println("Friday");
			break;
			
		case 6:
			System.out.println("Saturday");
			break;
			
		case 7:
			System.out.println("Sunday");
			break;

		default:
			System.out.println("wrong day");
			break;
		}
	}

}
