package com.codistan.java.bulent.lesson11b;

public class Student extends Youth implements BaskerballPlayer, SoccerPlayer{
	
	public void study() {
		System.out.println("Student is studying");

	}

	@Override
	public void kickBall() {
		System.out.println("Student is kicking the ball");
		
	}

	@Override
	public void jumpHigh() {
		System.out.println("Student is jumping high");
		
	}
}
