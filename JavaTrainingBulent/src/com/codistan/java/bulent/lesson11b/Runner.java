package com.codistan.java.bulent.lesson11b;

public class Runner {
	
	public static void main(String[] args) {
		
		Student obj1 = new Student();
		Human obj2 = new Student();//polymorphism
		
		obj1.study();
		obj1.bike();
		obj1.eat();
		
		obj2.eat();
		
		SoccerPlayer obj3 = new Student();
		
		obj3.kickBall();
		
	}
}
