package com.codistan.java.bulent.lesson8;

import java.util.Scanner;

public class SwitchStatement {

	public static void main(String[] args) {
		
		String grade = "C";
		switch(grade) {
		case "A":
			System.out.println("Perfect");
			break;
		case "B":
			System.out.println("Very good");
			break;
		case "C":
			System.out.println("You did Ok");
			break;
		default:
			System.out.println("Study harder");
			break;
		}
		
		Scanner keyboard = new Scanner(System.in);
		String category = keyboard.nextLine();
		switch(category) {
		case "A":
			System.out.println("You sit in the fron for $80");
			break;
		case "B":
			System.out.println("You sit in the back for $60");
			break;
		case "C":
			System.out.println("You sit in the balcony for $50");
			break;
		default:
			System.out.println("buy ticket");
			break;
		}
		}
	
	
}
