package com.codistan.java.bulent.lesson5;

public class imperialToMetric {

	public static void main(String[] args) {
		// we will give pound value and program will print the kg value.
		
		/*double poundValue = 5*0.453;
		double kg = poundValue;*/
		
		double poundValue = 5;
		double poundToKgConstant = 0.4535; //final makes a variable constant 
		double resultKg =poundValue * poundToKgConstant;
		
		//we will give inch value and program will print the cantimeter value
		
		double inchValue = 5 * 2.54;
		double cm = inchValue;
		
		//we will give mile value and program will calculate the kilometer value
		
		double mileValue = 5 * 1.6;
		double km = mileValue;
		
		// ons to ml
		
		double onsValue = 5 * 29.57;
		double ml = onsValue;
		
		//fahreneith to celcius
		
		double fhValue = 5;
		double celcius = ((fhValue - 32) * 5/9);
				
		System.out.println("5 pound is: " + resultKg + "kg");
		System.out.println("5 inchs is : " + cm + "cm");
		System.out.println("5 mile is: " + km + "km");
		System.out.println("5 ons is: " + onsValue + "ml");
		System.out.println("5 fahreneit is:" +  celcius + " celcius");

	
		
	}
	

}
