package com.codistan.java.bulent.lesson5;

public class PrimitiveVariablesAssignment {

	public static void main(String[] args) {
		/*
		 * create 2 int variables and assign 3 and 5 to them. create 2 more int variables and assign the result of multiplication of 5 and previous 2 integers. print 30% of the value of all those integers to the console in decimal numbers and then print if all these number or less than 10 (true or false).  
		 */
		
		int a = 3; 
		int b = 5; 
		int c = a*5; 
		int d = b*5; 
		
		System.out.println(a* ((double) 30/100));
		System.out.println(b* ((double) 30/100));
		System.out.println(c* ((double) 30/100));
		System.out.println(d* ((double) 30/100));
		
		System.out.println(a<10);
		System.out.println(b<10);
		System.out.println(c<10);
		System.out.println(d<10);


	}

}

	
	

	

