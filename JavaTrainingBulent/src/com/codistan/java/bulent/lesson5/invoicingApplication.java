package com.codistan.java.bulent.lesson5;

public class invoicingApplication {

	public static void main(String[] args) {
		/* Create a java class as Invoicing which will have variables for item name, item price, quantity and discount 
		percentage so it will calculate and print the total price (sales tax will be 10.25%).*/
		
		String itemName = "Apple";
		int itemPrice = 2;
		int itemQuantity = 3;
		double discountPercentage = 0.35;
		double tax = 1.1025;
		
		double beforeDiscount = itemPrice * itemQuantity * tax;
		
		System.out.println(itemName + " total price is " + (beforeDiscount - discountPercentage));
		
				
	}

}
