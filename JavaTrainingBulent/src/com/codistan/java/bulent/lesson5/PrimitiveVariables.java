package com.codistan.java.bulent.lesson5;

public class PrimitiveVariables {
	
	public static void main(String[] args) {
	System.out.println("We are about to start learning primitive variables");
	
	byte userAge = 56;
	System.out.println("The age of the user is " + userAge);
	
	short numberOfStudent = 87;
	System.out.println("The number of students " + numberOfStudent);
	
	int numberOfEmployees = 160_000;
	//160000 and 160_000 are same
	System.out.println("Number of employees is " + numberOfEmployees);
	
	long numberOfInhabitant = 25_000_000_000_000_000L;
	System.out.println("The number of inhabitants is " + numberOfInhabitant);
	
	float hourlyRate = 45.50f;
	System.out.println("The hourly wage of the employee is " + hourlyRate);
	
	double numberOfHours = 38.3;
	System.out.println("The weekly hours of the employee is " + numberOfHours);
	
	boolean passedExam = true;
	System.out.println("I have passed  ehe exam: " + passedExam);
	
	char grade = 'A'; //we can use unicode also '\u0041'
	System.out.println("My grade is " + grade);
	
	
	int a = 10;
	int b = 3;
	double a2 = a;
	double b2 = b;
	short a3 = (short) a; //casting example
	short b3 = (short) b;
	
	System.out.println(a*b); //multipation operator
	System.out.println((double)a/b); //casting example
	System.out.println(a%b); //modulus	System.out.println(a*b); //multipation operator
	System.out.println(a+b); 
	System.out.println(a-b);
	System.out.println("Adittion of a and b is " + a+b); //Concatenation
	System.out.println(a==b); //is equal operator
	System.out.println(a!=b); //is not equal operator
	System.out.println(a==b);
	
	a = a+1;
	System.out.println(a); //11
	
	a = a-1;
	System.out.println(a); //10
	System.out.println(++a); //11

	
//	a++; //post increase by one
//	++a; //pre increase by one
	
	a = a+5;
	System.out.println(a); //16
	a+=5;
	System.out.println(a); //21
	a-=5;
	System.out.println(a); //16
	a*=5;
	System.out.println(a);
	a/=5;
	System.out.println(a);
	a%=5;
	System.out.println(a);
	
	boolean iskadirAvailable = true;
	boolean isTayfurAvaible = false;
	boolean someoneWillTeachToday = (iskadirAvailable || isTayfurAvaible);
	System.out.println("Some one will teach today: " + someoneWillTeachToday);
	
	boolean todayNotHoliday = false;
	boolean thereIsClassSection = false;
	boolean iWillTeachToday = (todayNotHoliday && thereIsClassSection);
	System.out.println("I will teach today: " + iWillTeachToday);
	
	System.out.println(a<b);
	System.out.println(a<=b);
	System.out.println(a>b);
	System.out.println(a>=b);
	
	
	
	
	

	





	






	
	
	}
}