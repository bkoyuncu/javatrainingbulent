package com.codistan.java.bulent.lesson5;

public class salaryCalculator {

	public static void main(String[] args) {
		/* Create a class called SalaryCalculator which will calculate weekly total salary. 
		An employee can get $13.25 per hour but if he/she works more than 40 hours than 
		the wage will be calculated by x1.5 so more than regular wage. 
		Application will calculate total amount based on the hours an employee worked and print it to the console.*/
		
		int regularHour = 40;
		double perHour = 13.5;
		double totalRegularHour = regularHour * perHour;
		
		int overTime = 22;
		double overTimePerHour = perHour * 1.5;
		double totalOverTime = overTime * overTimePerHour;
		
		System.out.println("Your weekly earning is: $" + (totalRegularHour + totalOverTime));
		
		
	}

}
