package com.codistan.java.bulent.lesson5;

public class mpgtoLiterPerOneHundredKm {

	public static void main(String[] args) {
	//Given miles driven and burned gas in galons, calculate liter per 100 kilometers.
	// if your car mpg is 22, then liter per 100 km should be 10.6916
	//235.215/(22 US mpg) = 10.692 L/100 km
		
	double mpg = 22;
	
	double results = (235.215*100) / mpg;
	
	System.out.println("This car is burning " + results + " liters/100km");
	}

}
 